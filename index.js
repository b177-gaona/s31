// Setup dependencies
const express = require("express");
const mongoose = require('mongoose');
const taskRoute = require("./routes/taskRoute");

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// MongoDB connection
mongoose.connect("mongodb+srv://ky13_0pr0j3ct:QDukyuzuFSxJtjjX@cluster0.4kk1v.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use("/tasks", taskRoute);

// Server listening
app.listen(port, () => console.log(`Now listening to port ${port}`));

