// 1. Create a route for getting a specific task.
// 2. Create a controller function for retrieving a specific task.
// 3. Return the result back to the client/Postman.
// 4. Process a GET request at the "/tasks/:id" route using postman to get a specific task.
// 5. Create a route for changing the status of a task to "complete".
// 6. Create a controller function for changing the status of a task to "complete".
// 7. Return the result back to the client/Postman.
// 8. Process a PUT request at the "/tasks/:id/complete" route using postman to update a task.
// 9. Commit the changes with a message of Add activity code and push to gitlab.
// 10. Add the link in Boodle.

const express = require("express");

// Create a router instance that functions as a routing system
const router = express.Router();

// Import the taskControllers
const taskController = require("../controllers/taskControllers");

// Route to get all the tasks
// endpoint: localhost:3001/tasks
router.get("/", (req, res) => {
    taskController.getAllTasks().then(resultFromController => res.send (resultFromController
    ));
})

// Route to create a task
router.post("/", (req, res) => {
    taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// endpoint: localhost:3001/tasks/123456
router.delete("/:id", (req,res) => {
    taskController.deleteTask(req.params.id).then(resultFromController=>res.send(resultFromController));
})

// Route to update a task
router.put("/:id", (req,res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// Route to get specific task
router.get("/:id", (req, res) => {
    taskController.getTask(req.params.id).then(resultFromController => res.send (resultFromController
    ));
})

// Route to update a task status to complete
router.put("/:id/complete", (req,res) => {
    taskController.updateTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Export the router object to be used in index.js
module.exports = router;
